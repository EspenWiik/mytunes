﻿
using myTunesDemo.Models;
using myTunesDemo.Models.CustomerModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace myTunesDemo.Repositories
{
    public class CustomerRepository
    {
        //Get all customers in the database
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);                                
                                if (!reader.IsDBNull(5)) customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                                custList.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw (ex);
            }
            return custList;
        }

        // Get customer by id from database
        public Customer GetCustomer(string id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerID, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer" +
                " WHERE CustomerID = @CustomerID";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.Country = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.PostalCode = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw (ex);
            }
            return customer;
        }

        //Adds customer based on the customer model to the database
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw (ex);
            }
            return success;
        }

        //Updates existing customer in the database
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName," +
                " Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email" +
                " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw (ex);
            }
            return success;
        }

        //Gets list of countries with number of customers in that country ordered descending
        public List<CountryCustomerCount> GetCountriesByCustomerCount()
        {
            List<CountryCustomerCount> countryList = new List<CountryCustomerCount>();
            string sql = "SELECT COUNT(CustomerID), Country "+
                            "FROM Customer "+
                            "GROUP BY Country "+
                            "ORDER BY COUNT(CustomerID) DESC; ";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CountryCustomerCount countryCustomerCount = new CountryCustomerCount();
                                countryCustomerCount.Customers = reader.GetInt32(0);
                                countryCustomerCount.Country = reader.GetString(1);
                                countryList.Add(countryCustomerCount);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            return countryList;
        }

        //Gets the genres the customer has most tracks with
        public List<MostPopularGenre> GetMostPopularGenre(int id)
        {
            List<MostPopularGenre> popularGenre = new List<MostPopularGenre>();
            string sql = @"SELECT TOP 1 WITH TIES i.CustomerId,g.Name as Genre, COUNT(*) AS TrackCount
                FROM Invoice as i
                INNER JOIN InvoiceLine as il ON i.InvoiceId = il.InvoiceId
                INNER JOIN Track as t ON il.TrackId = t.TrackId
                INNER JOIN Genre as g ON t.GenreId = g.GenreId
                WHERE i.CustomerId = @CustomerId
                GROUP BY i.CustomerId, g.Name
                ORDER BY TrackCount DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                MostPopularGenre topGenre = new MostPopularGenre();
                                topGenre.CustomerId = reader.GetInt32(0);
                                topGenre.GenreName = reader.GetString(1);
                                topGenre.TrackCount = reader.GetInt32(2);
                                popularGenre.Add(topGenre);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;

            }
            return popularGenre;
        }

        public List<Invoice> GetHighestInvoice()
        { //Adds all invoice amounts and customerID to a list

            //List of <Track> which will be populated and returned
            List<Invoice> invoiceTotals = new List<Invoice>();

            // SQL string that sums all total values for each unqiue user in new column.
            // Also get customer name from Customer table
            string sql = "SELECT SUM(Invoice.Total) AS[TotalAmount], Customer.LastName, Invoice.CustomerId " +
                            "FROM Invoice INNER JOIN Customer " +
                            "ON Invoice.CustomerId = Customer.CustomerId " +
                            "GROUP BY Invoice.CustomerId, Customer.LastName "+
                            "ORDER BY TotalAmount DESC";

            try
            {
                // SQL connection
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();

                    // SQL Command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //SQL reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // (1)New Track, (2)assign values, (3)add to List
                            while (reader.Read())
                            {
                                Invoice invoice = new Invoice();
                                invoice.Total = reader.GetDecimal(0);
                                invoice.Name = reader.GetString(1);
                                invoice.CustomerId = reader.GetInt32(2);

                                invoiceTotals.Add(invoice);

                            }
                        }
                    }

                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }



            return invoiceTotals; 
        }

    }
}
