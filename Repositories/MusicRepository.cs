﻿using myTunesDemo.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Repositories
{
    public class MusicRepository
    {
        // Random number generator
        Random random = new Random();

        // Get 5 random Artists Method
        public List<Artist> GetRandomArtists()
        {
            //List of <Artist> which will be populated and returned
            List<Artist> artistList = new List<Artist>();

            //List of 5 random numbers "ids"
            List<int> ids = new List<int>();
            for (int i = 0; i < 5; i++)
            {

                int id = random.Next(275);
                ids.Add(id);

                // SQL command string, gets Artist where ArtistId = current random number
                string sql = $"SELECT ArtistId, Name FROM Artist WHERE ArtistId = {ids[i]}";
                try
                {
                    // SQL connection
                    using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                    {
                        conn.Open();

                        // SQL Command
                        using (SqlCommand cmd = new SqlCommand(sql, conn))
                        {
                            //SQL reader
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    // (1)New Artist, (2)assign values, (3)add to List
                                    Artist temp = new Artist();
                                    temp.ArtistId = reader.GetInt32(0);
                                    temp.Name = reader.GetString(1);
                                    artistList.Add(temp);
                                }
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                    throw (ex);
                }
            }

            return artistList;
        }

        //Get 5 random Genres Method
        public List<Genre> GetRandomGenres()
        {
            //List of <Genre> which will be populated and returned
            List<Genre> genreList = new List<Genre>();

            //List of 5 random numbers "ids"
            List<int> ids = new List<int>();
            for (int i = 0; i < 5; i++)
            {
                //adds a random number to the list
                int id = random.Next(25);
                ids.Add(id);

                // SQL command string, gets Genre where GenreId = current random number
                string sql = $"SELECT GenreId, Name FROM Genre WHERE GenreId = {ids[i]}";
                try
                {
                    // SQL connection
                    using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                    {
                        conn.Open();

                        //SQL Command
                        using (SqlCommand cmd = new SqlCommand(sql, conn))
                        {
                            //SQL Reader
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    // (1)New Genre, (2)assign values, (3)add to list
                                    Genre temp = new Genre();
                                    temp.GenreId = reader.GetInt32(0);
                                    temp.Name = reader.GetString(1);
                                    genreList.Add(temp);
                                }
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                    throw (ex);
                }
            }

            return genreList;
        }

        // Get 5 random Tracks method
        public List<Track> GetRandomTracks()
        {
            //List of <Track> which will be populated and returned
            List<Track> trackList = new List<Track>();

            //List of 5 random numbers "ids"
            List<int> ids = new List<int>();
            for (int i = 0; i < 5; i++)
            {

                int id = random.Next(1000);
                ids.Add(id);

                // SQL command string, gets Track where TrackId = current random number
                string sql = $"SELECT TrackId, Name FROM Track WHERE TrackId = {ids[i]}";
                try
                {
                    // SQL connection
                    using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                    {
                        conn.Open();

                        // SQL Command
                        using (SqlCommand cmd = new SqlCommand(sql, conn))
                        {
                            //SQL reader
                            using (SqlDataReader reader = cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    // (1)New Track, (2)assign values, (3)add to List
                                    Track temp = new Track();
                                    temp.TrackId = reader.GetInt32(0);
                                    temp.Name = reader.GetString(1);
                                    trackList.Add(temp);
                                }
                            }
                        }
                    }
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Message);
                    throw (ex);
                }
            }
            return trackList;
        }

        // Search for track by name Method
        public List<DetailedTrack> SearchResults(string searchString)
        {
            List<DetailedTrack> trackList = new List<DetailedTrack>();

            // SQL command string, gets Track information for tracks where Name contains the string paramter
            string sql = $"SELECT Track.Name, Album.Title, Genre.Name, Track.Composer " +
                         $"FROM Track " +
                         $"INNER JOIN Genre ON Track.GenreId = Genre.GenreId " +
                         $"INNER JOIN Album ON Track.AlbumId = Album.AlbumId " +
                         $"WHERE Track.Name LIKE '%{searchString}%'";

            try
            {
                // SQL connection
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();

                    // SQL Command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //SQL reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {


                                // (1)New DetaieldTrack, (2)assign values. (3) adds to list
                                DetailedTrack temp = new DetailedTrack();
                                
                                temp.Name = reader.GetString(0);
                                if (!reader.IsDBNull(1)) temp.AlbumName = reader.GetString(1);
                                if (!reader.IsDBNull(2)) temp.Genre = reader.GetString(2);
                                if (!reader.IsDBNull(3)) temp.Composer = reader.GetString(3);

                                // adds DetailedTrack to trackList
                                trackList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw (ex);
                // Log to console
            }


            return trackList;
        }
    }
}
