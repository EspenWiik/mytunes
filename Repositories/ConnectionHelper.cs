﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Repositories
{
    public class ConnectionHelper
    {
        public static string GetConnectionstring()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = "DESKTOP-8LVG2AB\\SQLEXPRESS"; // server
            connectionStringBuilder.InitialCatalog = "Chinook"; //database
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.MultipleActiveResultSets = true; // allows multiple SqlReaders at once
            return connectionStringBuilder.ConnectionString;
        }
    }
}
