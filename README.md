# myTunes
Please dont sue me apple

## Description
Using the Chinook database, this ASP.Net REST API opens up endpoints with valuable data for the fututre development of a iTunes like application.
Included, but not limited to, are endpoints that showcase random genres + artist + tracks, search for tracks by name, must popular genre for each customer etc.

## Usage

Get Chinook Database from:
https://github.com/lerocha/chinook-database and follow their instructions.

* Clone or download this project
* Open in Visual Studio
* In Repositories/ConnectionHelper change the following;
```
connectionStringBuilder.DataSource = <Your_SQL_server_name>
```
*  Build solution
* Run

### Some endpoints to try out
```
https://localhost:44366/api/customers/highest/spenders
https://localhost:44366/api/music/search/track=bad
https://localhost:44366/api/customers
```
More can be found in the included Postman collection.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
