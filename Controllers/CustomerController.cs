﻿using Microsoft.AspNetCore.Mvc;
using myTunesDemo.Models;
using myTunesDemo.Models.CustomerModels;
using myTunesDemo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace myTunesDemo.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {

        private readonly CustomerRepository _customerRepository;

        public CustomerController(CustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        // GET: api/customers
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            // using CustomerRepository to fetch all customers
            return Ok(_customerRepository.GetAllCustomers());
        }

        // GET api/customers/5
        [HttpGet("{id}")]
        public ActionResult<Customer> Get(string id)
        {
            // using CustomerRepository to fetch a specific customer
            return Ok(_customerRepository.GetCustomer(id));
        }

        // POST api/customers
        [HttpPost]
        public ActionResult Post(Customer customer)
        {
            // using CustomerRepository to add a new customer 
            bool success = _customerRepository.AddNewCustomer(customer);
            return CreatedAtAction(nameof(Get), new { id = customer.CustomerId }, success);
        }

        // PUT api/customers/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, Customer customer)
        {
            // Check for bad request 
            if (id != customer.CustomerId)
            {
                return BadRequest();
            }
            // using CustomerRepository to update an existing customer 
            bool success = _customerRepository.UpdateCustomer(customer);
            return NoContent();
        }

        // GET api/customers/countries
        [HttpGet("countries")]
        public ActionResult<IEnumerable<CountryCustomerCount>> GetCustomersInCountries()
        {
            // using CustomerRepository to get countries with customer count 
            var countries = _customerRepository.GetCountriesByCustomerCount();
            if (countries == null)
            {
                return NotFound("Could not get countries by customer count");
            }
            return Ok(countries);
        }

        // GET api/customers/5/genre
        [HttpGet("{id}/genre")]
        public ActionResult<IEnumerable<MostPopularGenre>> GetPopularGenre(int id)
        {
            // using  CustomerRepository to fetch a specific customers most popular genre
            return Ok(_customerRepository.GetMostPopularGenre(id));
        }

        // Get customers who are the highest spenders
        [HttpGet("highest/spenders")]
        public ActionResult<IEnumerable<Invoice>> GetInvoice()
        {
            return Ok(_customerRepository.GetHighestInvoice());
        }
    }
}
