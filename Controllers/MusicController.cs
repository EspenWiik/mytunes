﻿using Microsoft.AspNetCore.Mvc;
using myTunesDemo.Models;
using myTunesDemo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace myTunesDemo.Controllers
{
    [Route("api/music")]
    [ApiController]
    public class MusicController : ControllerBase
    {
        
        // MusicController properties

        private readonly MusicRepository _musicRepository;

       
        // MusicController constructor which instanciates the required repositories and assigns them to their respective properties
        public MusicController( MusicRepository musicRepository)
        {

            _musicRepository = musicRepository;
        }

        
        // HttpGet function which returns a MusicSample model populated with lists of tracks, artists, genres.
        // Each list consist of 5 random entires
        // GET: api/home
        [HttpGet()]
        public ActionResult <IEnumerable<MusicSample>> Get()
        {

            // uses repositories to fetch tracks, artists & genres. 
            List<Track> tracks = _musicRepository.GetRandomTracks();
            List<Artist> artists = _musicRepository.GetRandomArtists();
            List<Genre> genres = _musicRepository.GetRandomGenres();

            return Ok(new MusicSample(tracks, artists, genres));


        } 
         

        // HttpGet SearchFunction which searches for tracks containing any part of the URL string after "SearchTrack="
        // eg. https://localhost:44366/api/home/search/track=bad
        [HttpGet("search/track={searchString}")]
        public ActionResult<IEnumerable<DetailedTrack>> SearchResults(string searchString)
        {
            // returns results from searchResult Repository, passes URL string variable as paramter
            return Ok(_musicRepository.SearchResults(searchString));
        }





    }
}

