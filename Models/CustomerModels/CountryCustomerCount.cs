﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Models.CustomerModels
{
    public class CountryCustomerCount
    {
        public string Country { get; set; }
        public int Customers { get; set; }
    }
}
