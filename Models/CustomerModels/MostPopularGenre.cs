﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Models.CustomerModels
{
    public class MostPopularGenre
    {
        public int CustomerId { get; set; }
        public string GenreName { get; set; }
        public int TrackCount { get; set; }
    }
}
