﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Models.CustomerModels
{
    public class Invoice
    {
        public int CustomerId { get; set; }
        public decimal Total { get; set; } 
        public string Name { get; set; }
    }
}
