﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Models
{
    public class Artist
    {
        // data model class for Artist
        public int ArtistId { get; set; }
        public string Name { get; set; }
    }
}
