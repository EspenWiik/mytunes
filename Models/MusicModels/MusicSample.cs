﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Models
{
    public class MusicSample
    {
        // data model class for MusicSample which contains Lists of Track, Artist & Genre
        public List<Track> Tracks {get; set;}
        public List<Artist> Artists { get; set; }
        public List<Genre> Genres { get; set; }

        // Constructor for MusicSample
        public MusicSample(List<Track> tracks, List<Artist> artists, List<Genre> genres)
        {
            Tracks = tracks;
            Artists = artists;
            Genres = genres;
        }
    }
}
