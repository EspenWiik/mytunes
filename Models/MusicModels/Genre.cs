﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Models
{
    public class Genre
    {
        // data model class for Genre
        public int GenreId { get; set; }
        public string Name { get; set; }
    }
}
