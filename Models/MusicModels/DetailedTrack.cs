﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Models
{
    public class DetailedTrack
    {
        // data model class for DetailedTrack
        public string Name { get; set; }
        public string Composer { get; set; }
        public string Genre { get; set; }
        public string AlbumName { get; set; }
    }
}
