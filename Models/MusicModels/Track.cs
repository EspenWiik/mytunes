﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myTunesDemo.Models
{
    public class Track
    {
        // data model class for Track
        public int TrackId { get; set; }
        public string Name { get; set; }

    }
}
